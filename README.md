[![pipeline status](https://gitlab.com/francois-rosato-ekino/x1-dev-config/badges/master/pipeline.svg)](https://gitlab.com/francois-rosato-ekino/x1-dev-config/commits/master)

# X1 webapp configuration files

## ./public/stb-conf
Ces fichiers permettent de modifier l'aiguillage des Boxes de dev.
Il est donc possible de faire pointer la webapp remote sur votre IP (sur le même réseau local) en éditant ces fichiers.

## ./public/webapp-config-ekino
Ce fichier permet de modifier la configuration de toutes les boxes de dev.

## Url
https://francois-rosato-ekino.gitlab.io/x1-dev-config/{path/file}.json
